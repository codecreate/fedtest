
$(document).ready(function() {
	// load local JSON
	loadJSON();
});

function loadJSON() {
	// load local JSON file
	var catData = $.getJSON("list.json", function(json) {
		sortsendJSON(json);
	})
	.success(function() {
		console.log( "success" );
	})
	.error(function() {
		console.log( "error" );
	})
	.complete(function() {
		console.log( "complete" );
	});
}

function sortsendJSON(json) {
	// sort JSON according to id node value
	var sorted = json.cats.sort(function(a, b) {return a.id - b.id});
	// stringify before sending
	var sortedString = JSON.stringify(sorted);
	//	
	$.ajax({
		url : "JSON_processor.php",
		type : 'post',
		data : {
			json : sortedString,
		},
		success : function(result) {
			console.log(result);
			$("#result").html(result);
		},
		error : function(errorThrown) {
			console.log(errorThrown);
		}
	})
	console.log('sorted and sent');
}