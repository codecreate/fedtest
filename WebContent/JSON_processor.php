<?php

// handle the loaded JSON and return the compiled string

if ( isset($_REQUEST) ) {
    //
    $json = $_REQUEST['json'];
    $catsArray = json_decode( $json, true );
    
    // compile the return string of HTML
    $responseString = '';
    foreach($catsArray as $cat) {
        // test for expected data and replace missing data with fallback
        $link = ($cat['link'] != '') ? $cat['link'] : 'http://google.com';
        $image = ($cat['image'] != '') ? $cat['image'] : 'img/dog.jpg';
        $heading = ($cat['heading'] != '') ? $cat['heading'] : 'Replacement Heading';
        $content = ($cat['content'] != '') ? $cat['content'] : 'Replacement lipsum Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam quam id.';
        
        $responseString .= '<div class="span6"><a target="_blank" href="'.$link.'"><img src="'.$image.'" alt="meow" /></a><div class="innercontainer"><h4 class="uppercase">'.$heading.'</h4><p>'.$content.'</p></div></div>';
    }
    echo $responseString;
}

?>