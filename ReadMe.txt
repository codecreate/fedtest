INSTRUCTIONS FOR PHP FULL STACK DEVELOPER TEST
 
Your job is to implement code based on the supplied design, while keeping in mind that that is needs to be easy to maintain in the future.
 
You have 2 hours and it's up to you how to best use this time.

If possible, try to complete work in one sitting. Your work must be submitted via bitbucket repository. Please follow best practice for a git workflow.
 
Some guidelines:
* The layout must be responsive. 
* Use of SCSS in modular way is recommended. Feel free to include any existing grid like bootstrap grid to be part of your SCSS component
* Print Featured Cats list from JSON Data supplied in "list.json" using PHP
* Sort cats by "ID" associated with it before printing using Javascript
* Document test cases
* Incorporate test cases for fallbacks in JSON data 
 
Fonts used:
Heading: https://fonts.google.com/specimen/Montserrat
Content: Georgia, serif
 
JSON Data:
Please use supplied file data as your JSON data.
 
Best of luck!